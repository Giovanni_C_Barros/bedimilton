CREATE DATABASE atividadeDois;

CREATE TABLE jogador(
idjog int auto_increment,
nome varchar(60) NOT NULL,
PRIMARY KEY(idjog);
);

CREATE TABLE pontos(
Qjogador varchar(50) not null,
equipe varchar(25) not null,
set int(2) not null,
tipo varchar(20) not null,
jogada varchar(20) not null,
idpt int auto_increment,
primary key(idpt);
);

CREATE TABLE auxiliar tecnico(
idat int auto_increment,
nomeat varchar(60),
cpf int(12) not null,
residencia varchar(250),
telefone int (12)
primary key(idat);
);
CREATE TABLE apoio(
idapoio int auto_increment,
nomea varchar(60) not null,
residenciaAp varchar(70) not null,
telefoneA int (12),
primary key(idapoio);
);
CREATE TABLE tecnico(
idTec int auto_increment,
nomeTec varchar(60),
cpf int(12),
residenciaTec varchar(70),
primary key(idtec);
);

CREATE TABLE time(
id int auto_increment,
time varchar(20),
jogadorUm varchar(60),
jogadorDois varchar(60),
ptsDaPartida int(2),
primary key(id),
);

CREATE TABLE auxiliar tecnico(
idat int auto_increment,
nomeat varchar(60),
cpf int(12),
residenciAt varchar(60),
telefoneAt int (12),
primary key(idat);
);

CREATE TABLE partidas(
idp int auto_increment,
local varchar(60),
data varchar(10),
hora varchar(5),

timeUm varchar(30),
timeDois varchar(30),
auxiliarTecnico varchar(60),
juiz varchar(60))
);