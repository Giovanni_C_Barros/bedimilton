package modelo;

public class PartidaModelo {
private int idp;
private String local;
private String timeUM;
public String getTimeUM() {
	return timeUM;
}
public void setTimeUM(String timeUM) {
	this.timeUM = timeUM;
}
public String getTimeDois() {
	return timeDois;
}
public void setTimeDois(String timeDois) {
	this.timeDois = timeDois;
}
public String getAuxiliarTecnico() {
	return auxiliarTecnico;
}
public void setAuxiliarTecnico(String auxiliarTecnico) {
	this.auxiliarTecnico = auxiliarTecnico;
}
public String getJuiz() {
	return juiz;
}
public void setJuiz(String juiz) {
	this.juiz = juiz;
}
private String timeDois;
private String auxiliarTecnico;
private String juiz;
public String getLocal() {
	return local;
}
public void setLocal(String local) {
	this.local = local;
}
public String getData() {
	return Data;
}
public void setData(String data) {
	Data = data;
}
public String getHora() {
	return hora;
}
public void setHora(String hora) {
	this.hora = hora;
}
private String Data;
private String hora;
public int getIdp() {
	return idp;
}
public void setIdp(int idp) {
	this.idp = idp;
}
}
