package controle;
import java.io.IOException;  
import java.io.PrintWriter;  
import javax.servlet.ServletException;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;  
import javax.servlet.http.HttpSession; 
import javax.servlet.RequestDispatcher;  
import javax.servlet.annotation.WebServlet;

@WebServlet("/ServLogin")
public class ServLogin extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public ServLogin() {
        super();
    } 
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {
        response.setContentType("text/html");  
        PrintWriter out = response.getWriter();
        
        
        String u = request.getParameter("nome");
        String s = request.getParameter("senha");

        if (UsuarioControle.logar(u,s)){
            HttpSession session=request.getSession();  
            session.setAttribute("nome",u);  

            RequestDispatcher rd=request.getRequestDispatcher("home.jsp");  
            rd.forward(request,response);  
        
        }else if (!UsuarioControle.logar(u,s)) {
        	out.print("Senha ou usuario incorretos");
        	request.getRequestDispatcher("login.jsp").forward(request,response);
        }
        out.close();  
        }  
    }  