package controle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.PartidaModelo;
public class PartidaControle {
	public boolean inserirPartida(PartidaModelo partida){
	boolean retorno = false;
	try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO partidas(local,data,hora,timeUm,timeDois,auxiliarTecnico,juiz) VALUES(?,?,?,?,?,?,?)");
		ps.setString(1, partida.getLocal());
		ps.setString(2, partida.getData());
		ps.setString(3, partida.getHora());
		ps.setString(4, partida.getTimeUM());
		ps.setString(5, partida.getTimeDois());
		ps.setString(6, partida.getAuxiliarTecnico());
		ps.setString(7, partida.getJuiz());
		if(ps.execute()) {
			retorno=true;
		}
		con.fecharConexao();
}catch(SQLException e) {
	System.out.println("Erro de SQL:"+ e.getMessage());
}
	return retorno;
	}
}