package controle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import modelo.UsuarioModelo;
public class UsuarioControle {
	public boolean registrarUsuario(UsuarioModelo um){
	boolean retorno = false;
	try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("INSERT INTO usuario(nome,senha,cargo) VALUES(?,?,?)");
		ps.setString(1, um.getNomeUsuario());
		ps.setString(2, um.getSenha());
		ps.setString(3, um.getCargo());
		if(ps.execute()) {
			retorno=true;
		}
		con.fecharConexao();
}catch(SQLException e) {
	System.out.println("Erro de SQL:"+ e.getMessage());
}
	return retorno;
	}
	public static boolean logar(String user,String pass){  
		boolean retorno=false;  
	try {
		Conexao con = new Conexao();
		PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM usuario WHERE nome=? AND senha=?");
		ps.setString(1, user);
		ps.setString(2, pass);
		if(ps.execute()) {
			retorno=true;
		}
		con.fecharConexao();
}catch(SQLException e) {
	System.out.println("Erro de SQL:"+ e.getMessage());
}
	return retorno;
	}


}