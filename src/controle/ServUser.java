package controle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.UsuarioModelo;


@WebServlet("/ServUser")
public class ServUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ServUser() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
    	UsuarioModelo user = new UsuarioModelo();
    	user.setNomeUsuario(request.getParameter("nome"));
    	user.setSenha(request.getParameter("senha"));
    	user.setCargo(request.getParameter("cargo"));
    	if((!request.getParameter("nome").isEmpty()) &&  !request.getParameter("senha").isEmpty() && !request.getParameter("cargo").isEmpty()){

    	new UsuarioControle().registrarUsuario(user);
    		request.getRequestDispatcher("home.jsp").forward(request,response);
    	}
	}
}