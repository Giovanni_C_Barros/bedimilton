<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Iterator"%>
<%@page import="controle.Ler"%>
<%@ page import="java.util.List"%>
<%@page import="modelo.TimesModelo"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
HttpServletRequest httpServletRequest = (HttpServletRequest) request;
String url = httpServletRequest .getRequestURI();
HttpSession sessao = httpServletRequest.getSession();
if(sessao.getAttribute("nome")==null) {
	request.getRequestDispatcher("login.jsp").forward(request, response);
}
%>
<head>

<meta charset='UTF-8'>
<title>Tela principal</title>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport'
	content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'>
<link rel='stylesheet' type='text/css'
	href='Visual/css/bootstrap.min.css'>
<link rel='stylesheet' type='text/css' href='Visual/css/style.css'>

</head>
<body>
	<div class=" bg-container">
		<div class="container h-100vh">
			<div class="inicio text-center">
				<h1 class="inicio-2 ">Badminton</h1>
			</div>
			<div class="d-flex justify-content-center mt-4">
				<div class=" cc-1 card">
					<div class="card-body shadow">
						<form action="ServTimes" method='post'>
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="equipe">Nome do Time:</label>
								</div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name='equipe'
										placeholder="Nome do Time">
								</div>
							</div>
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="jogadores">Membros do time :</label>
								</div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name='jogadorUm'
										placeholder="Jogador Um"> <input type="text"
										class="form-control" name='jogadorDois'
										placeholder="Jogador Dois">

								</div>
							</div>
							<div class="d-flex justify-content-end " style="padding: 2rem;">
								<input class="btn btn-primary btn-xl js-scroll-trigger"
									type='submit' name='submit' value='Registrar time' />
									
						<a class="btn btn-primary" href="dorifto.jsp" role="button">Alterar dados de time</a>
						<a class="btn btn-primary" href="ServLogout" role="button">Logout</a>	
						<a class="btn btn-primary" href="partida.jsp" role="button">Marcar partida</a>	
							</div>
						</form>		
<%
Ler lr = new Ler(); 
List<TimesModelo> list =lr.get_values();
Iterator<TimesModelo> it=list.iterator();
%>
<table border="1">
<thead>
	<tr>
		<th>Id</th>
		<th>Nome do time</th>
		<th>Jogador 1</th>
		<th>Jogador 2</th>
		</tr>
</thead>
<tbody>
<%
while(it.hasNext()){
	TimesModelo tm=new TimesModelo();
	tm=it.next();
%>

<tr>
<td><%=tm.getId() %></td>
<td><%=tm.getTime() %></td>
<td><%=tm.getNomeJ() %></td>
<td><%=tm.getNomeJ2() %></td>
</tr>
<%
}
%>
</tbody>
</table>
</body>
</html>
