<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Iterator"%>
<%@page import="controle.Ler"%>
<%@ page import="java.util.List"%>
<%--<%@page import="java.awt.List"--%>
<%@page import="modelo.TimesModelo"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset='UTF-8'>
<title>Index</title>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport'
	content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'>
<link rel='stylesheet' type='text/css'
	href='Visual/css/bootstrap.min.css'>
<link rel='stylesheet' type='text/css' href='Visual/css/style.css'>

<title>Tela de login</title>
</head>
<body>


	<div class=" bg-container">
		<div class="container h-100vh">
			<div class="inicio text-center">
				<h1 class="inicio-2 ">Badminton</h1>
			</div>
			<div class="d-flex justify-content-center mt-4">
				<div class=" cc-1 card">
					<div class="card-body shadow">
						<form action="ServLogin" method='post'>
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="pessoa">Seu nome completo</label>
								</div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name='nome'
										placeholder="Seu nome">
								</div>
							</div>
							
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="pwd">Senha:</label>
								</div>	
								<div class="col-sm-7">
									<input type="password" class="form-control" name='senha'
										placeholder="Sua senha">

							<div class="d-flex justify-content-end " style="padding: 2rem;">
								<input class="btn btn-primary btn-xl js-scroll-trigger"
									type='submit' name='submit' value='Logar' />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>